package com.park_me.ws.spot.spotservice.exception;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestController
@ControllerAdvice
public class CustomResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(SpotNotAvailable.class)
    public final ResponseEntity<Object> handleSpotNotAvailable(Exception e, WebRequest request) {
        CustomExceptionResponse response = new CustomExceptionResponse(new Date(), e.getMessage(),
                "Parking Spot not available");
        return new ResponseEntity(response, HttpStatus.NOT_FOUND);
    }

}
