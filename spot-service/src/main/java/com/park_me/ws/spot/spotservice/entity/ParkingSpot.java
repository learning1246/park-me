package com.park_me.ws.spot.spotservice.entity;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.annotation.Id;
import com.park_me.ws.spot.spotservice.common.SPOT_TYPE;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "parking_spots")
public class ParkingSpot {

    @Id
    @Field("spotId")
    private Integer spotId;

    private Boolean available;

    private Integer level;

    private SPOT_TYPE type;
}
