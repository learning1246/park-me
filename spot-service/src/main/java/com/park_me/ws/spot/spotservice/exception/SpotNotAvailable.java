package com.park_me.ws.spot.spotservice.exception;

public class SpotNotAvailable extends RuntimeException {

    private String message;

    public SpotNotAvailable(String string) {
        this.message = string;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
