package com.park_me.ws.spot.spotservice.seq;

public interface SequenceDao {
    long getNextSequenceId(String key) throws SequenceException;
}
