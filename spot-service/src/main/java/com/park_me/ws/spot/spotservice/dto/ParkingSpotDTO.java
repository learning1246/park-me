package com.park_me.ws.spot.spotservice.dto;

import com.park_me.ws.spot.spotservice.common.SPOT_TYPE;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ParkingSpotDTO {

    private Integer spotId;

    private Boolean available;

    private Integer level;

    private SPOT_TYPE type;

}
