package com.park_me.ws.spot.spotservice.common;

public enum SPOT_TYPE {
    XSMALL,
    SMALL,
    LARGE,
    MEDIUM,
    VLARGE;
}
