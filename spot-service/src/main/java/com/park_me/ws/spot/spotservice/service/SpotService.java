package com.park_me.ws.spot.spotservice.service;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.park_me.ws.spot.spotservice.aop.SpotStatusTracker;
import com.park_me.ws.spot.spotservice.common.SPOT_TYPE;
import com.park_me.ws.spot.spotservice.dto.ParkingSpotDTO;
import com.park_me.ws.spot.spotservice.entity.ParkingSpot;
import com.park_me.ws.spot.spotservice.exception.SpotNotAvailable;
import com.park_me.ws.spot.spotservice.repository.SpotRepository;
import com.park_me.ws.spot.spotservice.seq.IdGeneratorKey;
import com.park_me.ws.spot.spotservice.seq.SequenceDao;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class SpotService {
    @Autowired
    SpotRepository repository;

    @Autowired
    SequenceDao sequenceDao;

    public Object save(ParkingSpotDTO dto) {
        ParkingSpot parkingSpot = new ParkingSpot();
        log.info("Add spot request: " + dto);
        BeanUtils.copyProperties(dto, parkingSpot);
        long nextSequenceId = sequenceDao.getNextSequenceId(IdGeneratorKey.SPOT.name());
        int spotId = (int) (dto.getLevel() * 100 + nextSequenceId);
        parkingSpot.setSpotId(spotId);
        log.info("Addded spot : " + parkingSpot);
        ParkingSpot save = repository.save(parkingSpot);
        return save;
    }

    public Object findById(Integer spotId) {
        log.info("Find spot by Id:" + spotId);
        ParkingSpot findById = repository.findBySpotId(spotId);
        if (findById == null) {
            log.error("Spot not found:" + spotId);
            throw new SpotNotAvailable(
                    "Spot not available");
        }
        ParkingSpotDTO parkingSpot = new ParkingSpotDTO();
        BeanUtils.copyProperties(findById, parkingSpot);
        return parkingSpot;
    }

    @SpotStatusTracker
    public void updateSpotStatus(Integer spotId, boolean isAvailable) {
        log.info("Updating spot status, Id:" + spotId + " Status: " + isAvailable);
        ParkingSpot findById = repository.findBySpotId(spotId);
        if (findById == null) {
            log.error("Spot not found:" + spotId);
            throw new SpotNotAvailable(
                    "Spot not available");
        }
        findById.setAvailable(isAvailable);
        repository.save(findById);
    }

    public Object deleteById(Integer spotId) {
        repository.deleteById(spotId);
        log.error("Deleting spot by Id:" + spotId);
        return "Slot Deleted";
    }

    public List<Object> findAll() {
        return repository.findAll().stream().map(entityToDtoMapper()).collect(Collectors.toList());
    }

    public List<Object> findAllAvailable() {
        return repository.findByAvailableTrue().stream().map(entityToDtoMapper()).collect(Collectors.toList());
    }

    public Function<? super ParkingSpot, ? extends ParkingSpotDTO> entityToDtoMapper() {
        Function<? super ParkingSpot, ? extends ParkingSpotDTO> mapper = fromDb -> {
            ParkingSpotDTO parkingSpot = new ParkingSpotDTO();
            BeanUtils.copyProperties(fromDb, parkingSpot);
            return parkingSpot;
        };
        return mapper;
    }

    public List<Object> findAllAvailableByType(SPOT_TYPE type) {
        return repository.findByAvailableTrueAndType(type).stream().map(entityToDtoMapper())
                .collect(Collectors.toList());
    }

}
