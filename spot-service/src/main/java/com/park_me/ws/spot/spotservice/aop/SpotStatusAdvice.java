package com.park_me.ws.spot.spotservice.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class SpotStatusAdvice {

    @Around("@annotation(com.park_me.ws.spot.spotservice.aop.SpotStatusTracker)")
    public Object spotStatusChanged(ProceedingJoinPoint pjp) throws Throwable {
        // System.out.println(
        // "Class Name :" + pjp.getTarget().toString() + " Method name : " +
        // pjp.getSignature().getName());
        System.out.println("Slot Id: " + pjp.getArgs()[0] + " Status changing to: " + pjp.getArgs()[1]);
        Object o = pjp.proceed();
        return o;
    }
}
