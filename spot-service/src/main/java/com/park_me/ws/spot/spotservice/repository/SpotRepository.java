package com.park_me.ws.spot.spotservice.repository;

import java.util.Collection;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.park_me.ws.spot.spotservice.common.SPOT_TYPE;
import com.park_me.ws.spot.spotservice.entity.ParkingSpot;

public interface SpotRepository extends MongoRepository<ParkingSpot, Integer> {

    ParkingSpot findBySpotId(Integer spotId);

    Collection<ParkingSpot> findByAvailableTrue();

    Collection<ParkingSpot> findByAvailableTrueAndType(SPOT_TYPE type);

}
