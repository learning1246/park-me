package com.park_me.ws.spot.spotservice.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class AroundAdvice {

    @Pointcut(value = "execution(* com.park_me.ws.spot.spotservice.controller.*.findAllAvailableByType(..))")
    public void getByType_pjp() {

    }

    @Around("getByType_pjp()")
    public Object getAllByTypeAdvice(ProceedingJoinPoint pjp) throws Throwable {
        System.out.println(
                "Class  name:" + pjp.getTarget().toString() + " Method name : " + pjp.getSignature().getName());
        Object allByTypes = pjp.proceed();
        System.out.println("Invoked :" + allByTypes);
        return allByTypes;
    }
}
