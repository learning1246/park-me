package com.park_me.ws.spot.spotservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.park_me.ws.spot.spotservice.common.SPOT_TYPE;
import com.park_me.ws.spot.spotservice.dto.ParkingSpotDTO;
import com.park_me.ws.spot.spotservice.service.SpotService;

@RestController
@RequestMapping("/spots")
public class SpotController {

    @Autowired
    private SpotService spotService;

    @PostMapping("/add")
    public Object save(@RequestBody ParkingSpotDTO dto) {
        return spotService.save(dto);
    }

    @GetMapping("/{spotId}")
    public Object findById(@PathVariable Integer spotId) {
        return spotService.findById(spotId);
    }

    @PutMapping("/{spotId}/{isAvailable}")
    public void updateSpotStatus(@PathVariable Integer spotId, @PathVariable Boolean isAvailable) {
        spotService.updateSpotStatus(spotId, isAvailable);
    }

    @DeleteMapping("/{spotId}")
    public Object deleteById(@PathVariable Integer spotId) {
        return spotService.deleteById(spotId);
    }

    @GetMapping("/all")
    public List<Object> findAll() {
        return spotService.findAll();
    }

    @GetMapping("/available")
    public List<Object> findAllAvailable() {
        return spotService.findAllAvailable();
    }

    @GetMapping("/available/{type}")
    public List<Object> findAllAvailableByType(@PathVariable SPOT_TYPE type) {
        return spotService.findAllAvailableByType(type);
    }
}
