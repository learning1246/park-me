package com.park_me.ws.parkingservice.exception;

public class ParkingTransactionNotAvailable extends RuntimeException {

    private String message;

    public ParkingTransactionNotAvailable(String string) {
        this.message = string;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
