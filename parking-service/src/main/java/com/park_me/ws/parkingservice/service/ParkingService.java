package com.park_me.ws.parkingservice.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.park_me.ws.parkingservice.common.PAYMENT_STATUS;
import com.park_me.ws.parkingservice.common.SPOT_TYPE;
import com.park_me.ws.parkingservice.dto.ExitParkingRequest;
import com.park_me.ws.parkingservice.dto.ParkingRequest;
import com.park_me.ws.parkingservice.dto.ParkingResponse;
import com.park_me.ws.parkingservice.dto.ParkingSpotDTO;
import com.park_me.ws.parkingservice.entity.ParkingTransaction;
import com.park_me.ws.parkingservice.exception.BillNotCalculated;
import com.park_me.ws.parkingservice.exception.ParkingTransactionIsAlreadyComplete;
import com.park_me.ws.parkingservice.exception.ParkingTransactionNotAvailable;
import com.park_me.ws.parkingservice.exception.SpotNotAvailableForParking;
import com.park_me.ws.parkingservice.repository.ParkingRepository;
import com.park_me.ws.parkingservice.spot.SpotServiceClient;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ParkingService {

    @Autowired
    private SpotServiceClient spotService;

    @Autowired
    private ParkingRepository repository;

    @CircuitBreaker(name = "parking-service-cb", fallbackMethod = "getDefaultSpots")
    @Retry(name = "parking-service-retry")
    public List<ParkingSpotDTO> findAllAvailableByType(SPOT_TYPE type) {
        return spotService.findAllAvailableByType(type).stream().map(spot -> {
            spot.setRate(getCharge(spot.getType()));
            return spot;
        }).collect(Collectors.toList());
    }

    public List<Object> getDefaultSpots(Exception e) {
        return Arrays.asList();
    }

    public ParkingResponse assignParking(ParkingRequest request) {
        log.info("Assign parking request: " + request);
        ParkingTransaction transaction = new ParkingTransaction();
        ParkingSpotDTO parkingSpotDTO = spotService.findById(request.getSpotId());
        if (parkingSpotDTO == null || !parkingSpotDTO.getAvailable()) {
            log.error("Spot not available for parking: " + request);
            throw new SpotNotAvailableForParking(
                    "Spot not available for parking");
        }
        String transactionId = UUID.randomUUID().toString();
        transaction.setTransactionId(transactionId);
        transaction.setInTime(new Date());
        transaction.setSpotId(request.getSpotId());
        transaction.setVehicleNumber(request.getVehicleNumber());
        transaction.setPaymentStatus(PAYMENT_STATUS.PENDING);
        transaction.setSpotType(request.getSpotType());
        repository.save(transaction);
        spotService.updateSpotStatus(request.getSpotId(), false);
        log.info("Parking transaction generated: " + transactionId);
        return new ParkingResponse(transactionId);
    }

    public BigDecimal calculateBill(ExitParkingRequest request) {
        log.info("calculateBill request: " + request);
        ParkingTransaction transaction = repository.findById(request.getTransactionId()).get();
        if (transaction == null) {
            log.error("Parking transaction not available: " + request);
            throw new ParkingTransactionNotAvailable("Parking transaction not available");
        } else if (transaction.getPaymentStatus() == PAYMENT_STATUS.COMPLETE) {
            log.error("Parking transaction is already complete: " + request);
            throw new ParkingTransactionIsAlreadyComplete("Parking transaction is already complete");
        }
        return calculateBill(request, transaction);
    }

    @Retry(name = "parking-service-retry")
    private BigDecimal calculateBill(ExitParkingRequest request, ParkingTransaction transaction) {
        calculateTotal(transaction, request);
        repository.save(transaction);
        log.info("Parking transaction accepted: " + transaction);
        return transaction.getTotal();
    }

    public void calculateTotal(ParkingTransaction transaction, ExitParkingRequest request) {
        Date inTime = transaction.getInTime();
        Date outTime = new Date();
        long diff = outTime.getTime() - inTime.getTime();
        long minutes = TimeUnit.MILLISECONDS.toMinutes(diff);
        double subTotal = 0.0;
        int vehicleCharge = getCharge(transaction.getSpotType());
        long additionalMinutes = minutes - 15;
        if (additionalMinutes <= 0) {
            subTotal = vehicleCharge;
        } else {
            long quaterSlots = additionalMinutes / 15;
            long extraMinutes = additionalMinutes % 15;
            subTotal = vehicleCharge + (quaterSlots * vehicleCharge) + (extraMinutes * 0.1);
        }
        BigDecimal total = new BigDecimal(subTotal);
        total = total.setScale(2, RoundingMode.HALF_UP);
        transaction.setDuration(minutes);
        transaction.setOutTime(outTime);
        transaction.setTotal(total);
    }

    public int getCharge(SPOT_TYPE type) {
        switch (type) {
            case XSMALL:
                return 2;
            case SMALL:
                return 2;
            case MEDIUM:
                return 3;
            case LARGE:
                return 4;
            case XLARGE:
                return 5;
        }
        return 1;
    }

    public void completePayment(String transactionId) {
        log.info("Parking exit request received for Id: " + transactionId);
        ParkingTransaction transaction = null;
        try {
            transaction = repository.findById(transactionId).get();
        } catch (NoSuchElementException nse) {
            log.error("Parking transaction not available: " + transactionId);
            throw new ParkingTransactionNotAvailable("Parking transaction not available");
        }
        if (transaction == null) {
            log.error("Parking transaction not available: " + transactionId);
            throw new ParkingTransactionNotAvailable("Parking transaction not available");
        } else if (transaction.getPaymentStatus() == PAYMENT_STATUS.COMPLETE) {
            log.error("Parking transaction is already complete: " + transactionId);
            throw new ParkingTransactionIsAlreadyComplete("Parking transaction is already complete");
        } else if (transaction.getTotal() == null) {
            log.error("Can not exit, please calculate bill first: " + transactionId);
            throw new BillNotCalculated("Can not exit, please calculate bill first");
        }
        completePayment(transaction);
    }

    @Retry(name = "parking-service-retry")
    private void completePayment(ParkingTransaction transaction) {
        transaction.setPaymentStatus(PAYMENT_STATUS.COMPLETE);
        repository.save(transaction);
        spotService.updateSpotStatus(transaction.getSpotId(), true);
        // log.error("Parking transaction completed: " + transaction);
    }

}
