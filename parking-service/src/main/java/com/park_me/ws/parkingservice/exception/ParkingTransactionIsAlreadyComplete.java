package com.park_me.ws.parkingservice.exception;

public class ParkingTransactionIsAlreadyComplete extends RuntimeException {

    private String message;

    public ParkingTransactionIsAlreadyComplete(String string) {
        this.message = string;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
