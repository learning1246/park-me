package com.park_me.ws.parkingservice.controller;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.park_me.ws.parkingservice.common.SPOT_TYPE;
import com.park_me.ws.parkingservice.dto.ExitParkingRequest;
import com.park_me.ws.parkingservice.dto.ParkingRequest;
import com.park_me.ws.parkingservice.dto.ParkingResponse;
import com.park_me.ws.parkingservice.dto.ParkingSpotDTO;
import com.park_me.ws.parkingservice.service.ParkingService;

@RestController
@CrossOrigin
@RequestMapping("/parking")
public class ParkingController {

    @Autowired
    private ParkingService parkingService;

    @GetMapping("/available/{type}")
    public List<ParkingSpotDTO> findAllAvailableByType(@PathVariable SPOT_TYPE type) {
        return parkingService.findAllAvailableByType(type);
    }

    @PostMapping("/assign")
    public ParkingResponse assignParking(@Valid @RequestBody ParkingRequest request) {
        return parkingService.assignParking(request);
    }

    @PostMapping("/bill")
    public BigDecimal calculateBill(@Valid @RequestBody ExitParkingRequest request) {
        return parkingService.calculateBill(request);
    }

    @PatchMapping("/exit/{transactionId}")
    public String completePayment(@PathVariable String transactionId) {
        parkingService.completePayment(transactionId);
        return "you can now exit";
    }

}
