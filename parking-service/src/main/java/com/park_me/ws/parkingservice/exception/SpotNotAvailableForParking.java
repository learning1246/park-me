package com.park_me.ws.parkingservice.exception;

public class SpotNotAvailableForParking extends RuntimeException {

    private String message;

    public SpotNotAvailableForParking(String string) {
        this.message = string;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
