package com.park_me.ws.parkingservice.dto;

import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExitParkingRequest {

    @NotEmpty(message = "Transaction Id is required")
    private String transactionId;

}
