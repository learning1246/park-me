package com.park_me.ws.parkingservice.common;

public enum PAYMENT_STATUS {
    PENDING,
    COMPLETE;
}
