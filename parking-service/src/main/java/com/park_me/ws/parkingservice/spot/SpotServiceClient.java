package com.park_me.ws.parkingservice.spot;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import com.park_me.ws.parkingservice.common.SPOT_TYPE;
import com.park_me.ws.parkingservice.dto.ParkingSpotDTO;

@FeignClient(name = "SPOT-SERVICE")
public interface SpotServiceClient {

    @PostMapping("/spots/add")
    public Object save(ParkingSpotDTO dto);

    @GetMapping("/spots/{spotId}")
    public ParkingSpotDTO findById(@PathVariable Integer spotId);

    @PutMapping("/spots/{spotId}/{isAvailable}")
    public void updateSpotStatus(@PathVariable Integer spotId, @PathVariable Boolean isAvailable);

    @DeleteMapping("/spots/{spotId}")
    public ParkingSpotDTO deleteById(@PathVariable Integer spotId);

    @GetMapping("/spots/all")
    public List<ParkingSpotDTO> findAll();

    @GetMapping("/spots/available")
    public List<ParkingSpotDTO> findAllAvailable();

    @GetMapping("/spots/available/{type}")
    public List<ParkingSpotDTO> findAllAvailableByType(@PathVariable SPOT_TYPE type);
}
