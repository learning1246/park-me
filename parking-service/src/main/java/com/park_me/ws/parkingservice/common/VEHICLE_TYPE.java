package com.park_me.ws.parkingservice.common;

public enum VEHICLE_TYPE {
    XSMALL,
    SMALL,
    LARGE,
    MEDIUM,
    XLARGE;
}
