package com.park_me.ws.parkingservice.exception;

import java.util.Date;
import java.util.stream.Collectors;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestController
@ControllerAdvice
public class CustomResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(SpotNotAvailableForParking.class)
    public final ResponseEntity<Object> handleSpotNotAvailableForParking(Exception e, WebRequest request) {
        CustomExceptionResponse response = new CustomExceptionResponse(new Date(), e.getMessage(),
                "Parking Spot not available parking");
        return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ParkingTransactionNotAvailable.class)
    public final ResponseEntity<Object> handleTransactionNotAvailable(Exception e, WebRequest request) {
        CustomExceptionResponse response = new CustomExceptionResponse(new Date(), e.getMessage(),
                "Parking transaction not available parking");
        return new ResponseEntity(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ParkingTransactionIsAlreadyComplete.class)
    public final ResponseEntity<Object> handleTransactionIsAlreadyComplete(Exception e, WebRequest request) {
        CustomExceptionResponse response = new CustomExceptionResponse(new Date(), e.getMessage(),
                "Parking transaction is already complete");
        return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(BillNotCalculated.class)
    public final ResponseEntity<Object> handleBillNotCalculated(Exception e, WebRequest request) {
        CustomExceptionResponse response = new CustomExceptionResponse(new Date(), e.getMessage(),
                "Bill not calculated");
        return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
            HttpHeaders headers, HttpStatus status, WebRequest request) {
        CustomExceptionResponse response = new CustomExceptionResponse(new Date(), "Request Validation Failed",
                ex.getBindingResult().getAllErrors().stream().map(e -> e.getDefaultMessage())
                        .collect(Collectors.toList())
                        .toString());
        return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
            HttpHeaders headers, HttpStatus status, WebRequest request) {
        CustomExceptionResponse response = new CustomExceptionResponse(new Date(), "Request Validation Failed",
                ex.getCause().getLocalizedMessage());
        return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
    }

}
