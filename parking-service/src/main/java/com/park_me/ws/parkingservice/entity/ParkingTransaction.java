package com.park_me.ws.parkingservice.entity;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.park_me.ws.parkingservice.common.PAYMENT_STATUS;
import com.park_me.ws.parkingservice.common.SPOT_TYPE;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document("parking_transactions")
public class ParkingTransaction {

    @Id
    private String transactionId;

    private Integer spotId;

    private String vehicleNumber;

    private Date inTime;

    private Date outTime;

    private long duration;

    private BigDecimal total;

    private PAYMENT_STATUS paymentStatus;

    private SPOT_TYPE spotType;

}
