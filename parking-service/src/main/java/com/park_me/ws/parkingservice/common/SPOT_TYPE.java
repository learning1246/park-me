package com.park_me.ws.parkingservice.common;

public enum SPOT_TYPE {
    XSMALL,
    SMALL,
    LARGE,
    MEDIUM,
    XLARGE;
}
