package com.park_me.ws.parkingservice.exception;

public class BillNotCalculated extends RuntimeException {

    private String message;

    public BillNotCalculated(String string) {
        this.message = string;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
