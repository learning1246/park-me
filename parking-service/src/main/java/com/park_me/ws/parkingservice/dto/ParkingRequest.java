package com.park_me.ws.parkingservice.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.park_me.ws.parkingservice.common.SPOT_TYPE;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ParkingRequest {

    @NotNull(message = "Spot Id is required")
    private Integer spotId;

    @NotEmpty(message = "Vehicle number is required")
    private String vehicleNumber;

    @NotNull(message = "Spot type is required")
    private SPOT_TYPE spotType;

}
