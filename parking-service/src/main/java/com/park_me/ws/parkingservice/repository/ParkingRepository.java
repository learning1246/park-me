package com.park_me.ws.parkingservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.park_me.ws.parkingservice.entity.ParkingTransaction;

public interface ParkingRepository extends MongoRepository<ParkingTransaction, String> {

}
